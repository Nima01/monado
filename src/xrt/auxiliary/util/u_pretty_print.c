// Copyright 2022, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Pretty printing various Monado things.
 * @author Jakob Bornecrantz <jakob@collabora.com>
 * @ingroup aux_pretty
 */

#include "util/u_misc.h"
#include "util/u_pretty_print.h"

#include <string.h>
#include <stdarg.h>
#include <stdio.h>


/*
 *
 * Internal helpers.
 *
 */

#define DG(str) (dg.func(dg.ptr, str, strlen(str)))

const char *
get_xrt_input_type_short_str(enum xrt_input_type type)
{
	switch (type) {
	case XRT_INPUT_TYPE_VEC1_ZERO_TO_ONE: return "VEC1_ZERO_TO_ONE";
	case XRT_INPUT_TYPE_VEC1_MINUS_ONE_TO_ONE: return "VEC1_MINUS_ONE_TO_ONE";
	case XRT_INPUT_TYPE_VEC2_MINUS_ONE_TO_ONE: return "VEC2_MINUS_ONE_TO_ONE";
	case XRT_INPUT_TYPE_VEC3_MINUS_ONE_TO_ONE: return "VEC3_MINUS_ONE_TO_ONE";
	case XRT_INPUT_TYPE_BOOLEAN: return "BOOLEAN";
	case XRT_INPUT_TYPE_POSE: return "POSE";
	case XRT_INPUT_TYPE_HAND_TRACKING: return "HAND_TRACKING";
	default: return "<UNKNOWN>";
	}
}


/*
 *
 * 'Exported' functions.
 *
 */

void
u_pp(struct u_pp_delegate dg, const char *fmt, ...)
{
	// Should be plenty enough for most prints.
	char tmp[1024];
	char *dst = tmp;
	va_list args;

	va_start(args, fmt);
	int ret = vsnprintf(NULL, 0, fmt, args);
	va_end(args);

	if (ret <= 0) {
		return;
	}

	size_t size = (size_t)ret;
	// Safe to do because MAX_INT should be less then MAX_SIZE_T
	size_t size_with_null = size + 1;

	if (size_with_null > ARRAY_SIZE(tmp)) {
		dst = U_TYPED_ARRAY_CALLOC(char, size_with_null);
	}

	va_start(args, fmt);
	ret = vsnprintf(dst, size_with_null, fmt, args);
	va_end(args);

	dg.func(dg.ptr, dst, size);

	if (tmp != dst) {
		free(dst);
	}
}

void
u_pp_xrt_input_name(struct u_pp_delegate dg, enum xrt_input_name name)
{
	switch (name) {
	case XRT_INPUT_GENERIC_HEAD_POSE: DG("XRT_INPUT_GENERIC_HEAD_POSE"); return;
	case XRT_INPUT_GENERIC_HEAD_DETECT: DG("XRT_INPUT_GENERIC_HEAD_DETECT"); return;
	case XRT_INPUT_GENERIC_HAND_TRACKING_LEFT: DG("XRT_INPUT_GENERIC_HAND_TRACKING_LEFT"); return;
	case XRT_INPUT_GENERIC_HAND_TRACKING_RIGHT: DG("XRT_INPUT_GENERIC_HAND_TRACKING_RIGHT"); return;
	case XRT_INPUT_GENERIC_TRACKER_POSE: DG("XRT_INPUT_GENERIC_TRACKER_POSE"); return;
	case XRT_INPUT_SIMPLE_SELECT_CLICK: DG("XRT_INPUT_SIMPLE_SELECT_CLICK"); return;
	case XRT_INPUT_SIMPLE_MENU_CLICK: DG("XRT_INPUT_SIMPLE_MENU_CLICK"); return;
	case XRT_INPUT_SIMPLE_GRIP_POSE: DG("XRT_INPUT_SIMPLE_GRIP_POSE"); return;
	case XRT_INPUT_SIMPLE_AIM_POSE: DG("XRT_INPUT_SIMPLE_AIM_POSE"); return;
	case XRT_INPUT_PSMV_PS_CLICK: DG("XRT_INPUT_PSMV_PS_CLICK"); return;
	case XRT_INPUT_PSMV_MOVE_CLICK: DG("XRT_INPUT_PSMV_MOVE_CLICK"); return;
	case XRT_INPUT_PSMV_START_CLICK: DG("XRT_INPUT_PSMV_START_CLICK"); return;
	case XRT_INPUT_PSMV_SELECT_CLICK: DG("XRT_INPUT_PSMV_SELECT_CLICK"); return;
	case XRT_INPUT_PSMV_SQUARE_CLICK: DG("XRT_INPUT_PSMV_SQUARE_CLICK"); return;
	case XRT_INPUT_PSMV_CROSS_CLICK: DG("XRT_INPUT_PSMV_CROSS_CLICK"); return;
	case XRT_INPUT_PSMV_CIRCLE_CLICK: DG("XRT_INPUT_PSMV_CIRCLE_CLICK"); return;
	case XRT_INPUT_PSMV_TRIANGLE_CLICK: DG("XRT_INPUT_PSMV_TRIANGLE_CLICK"); return;
	case XRT_INPUT_PSMV_TRIGGER_VALUE: DG("XRT_INPUT_PSMV_TRIGGER_VALUE"); return;
	case XRT_INPUT_PSMV_GRIP_POSE: DG("XRT_INPUT_PSMV_GRIP_POSE"); return;
	case XRT_INPUT_PSMV_AIM_POSE: DG("XRT_INPUT_PSMV_AIM_POSE"); return;
	case XRT_INPUT_PSMV_BODY_CENTER_POSE: DG("XRT_INPUT_PSMV_BODY_CENTER_POSE"); return;
	case XRT_INPUT_PSMV_BALL_CENTER_POSE: DG("XRT_INPUT_PSMV_BALL_CENTER_POSE"); return;
	case XRT_INPUT_HYDRA_1_CLICK: DG("XRT_INPUT_HYDRA_1_CLICK"); return;
	case XRT_INPUT_HYDRA_2_CLICK: DG("XRT_INPUT_HYDRA_2_CLICK"); return;
	case XRT_INPUT_HYDRA_3_CLICK: DG("XRT_INPUT_HYDRA_3_CLICK"); return;
	case XRT_INPUT_HYDRA_4_CLICK: DG("XRT_INPUT_HYDRA_4_CLICK"); return;
	case XRT_INPUT_HYDRA_MIDDLE_CLICK: DG("XRT_INPUT_HYDRA_MIDDLE_CLICK"); return;
	case XRT_INPUT_HYDRA_BUMPER_CLICK: DG("XRT_INPUT_HYDRA_BUMPER_CLICK"); return;
	case XRT_INPUT_HYDRA_JOYSTICK_CLICK: DG("XRT_INPUT_HYDRA_JOYSTICK_CLICK"); return;
	case XRT_INPUT_HYDRA_JOYSTICK_VALUE: DG("XRT_INPUT_HYDRA_JOYSTICK_VALUE"); return;
	case XRT_INPUT_HYDRA_TRIGGER_VALUE: DG("XRT_INPUT_HYDRA_TRIGGER_VALUE"); return;
	case XRT_INPUT_HYDRA_POSE: DG("XRT_INPUT_HYDRA_POSE"); return;
	case XRT_INPUT_DAYDREAM_TOUCHPAD_CLICK: DG("XRT_INPUT_DAYDREAM_TOUCHPAD_CLICK"); return;
	case XRT_INPUT_DAYDREAM_BAR_CLICK: DG("XRT_INPUT_DAYDREAM_BAR_CLICK"); return;
	case XRT_INPUT_DAYDREAM_CIRCLE_CLICK: DG("XRT_INPUT_DAYDREAM_CIRCLE_CLICK"); return;
	case XRT_INPUT_DAYDREAM_VOLUP_CLICK: DG("XRT_INPUT_DAYDREAM_VOLUP_CLICK"); return;
	case XRT_INPUT_DAYDREAM_VOLDN_CLICK: DG("XRT_INPUT_DAYDREAM_VOLDN_CLICK"); return;
	case XRT_INPUT_DAYDREAM_TOUCHPAD: DG("XRT_INPUT_DAYDREAM_TOUCHPAD"); return;
	case XRT_INPUT_DAYDREAM_POSE: DG("XRT_INPUT_DAYDREAM_POSE"); return;
	case XRT_INPUT_DAYDREAM_TOUCHPAD_TOUCH: DG("XRT_INPUT_DAYDREAM_TOUCHPAD_TOUCH"); return;
	case XRT_INPUT_INDEX_SYSTEM_CLICK: DG("XRT_INPUT_INDEX_SYSTEM_CLICK"); return;
	case XRT_INPUT_INDEX_SYSTEM_TOUCH: DG("XRT_INPUT_INDEX_SYSTEM_TOUCH"); return;
	case XRT_INPUT_INDEX_A_CLICK: DG("XRT_INPUT_INDEX_A_CLICK"); return;
	case XRT_INPUT_INDEX_A_TOUCH: DG("XRT_INPUT_INDEX_A_TOUCH"); return;
	case XRT_INPUT_INDEX_B_CLICK: DG("XRT_INPUT_INDEX_B_CLICK"); return;
	case XRT_INPUT_INDEX_B_TOUCH: DG("XRT_INPUT_INDEX_B_TOUCH"); return;
	case XRT_INPUT_INDEX_SQUEEZE_VALUE: DG("XRT_INPUT_INDEX_SQUEEZE_VALUE"); return;
	case XRT_INPUT_INDEX_SQUEEZE_FORCE: DG("XRT_INPUT_INDEX_SQUEEZE_FORCE"); return;
	case XRT_INPUT_INDEX_TRIGGER_CLICK: DG("XRT_INPUT_INDEX_TRIGGER_CLICK"); return;
	case XRT_INPUT_INDEX_TRIGGER_VALUE: DG("XRT_INPUT_INDEX_TRIGGER_VALUE"); return;
	case XRT_INPUT_INDEX_TRIGGER_TOUCH: DG("XRT_INPUT_INDEX_TRIGGER_TOUCH"); return;
	case XRT_INPUT_INDEX_THUMBSTICK: DG("XRT_INPUT_INDEX_THUMBSTICK"); return;
	case XRT_INPUT_INDEX_THUMBSTICK_CLICK: DG("XRT_INPUT_INDEX_THUMBSTICK_CLICK"); return;
	case XRT_INPUT_INDEX_THUMBSTICK_TOUCH: DG("XRT_INPUT_INDEX_THUMBSTICK_TOUCH"); return;
	case XRT_INPUT_INDEX_TRACKPAD: DG("XRT_INPUT_INDEX_TRACKPAD"); return;
	case XRT_INPUT_INDEX_TRACKPAD_FORCE: DG("XRT_INPUT_INDEX_TRACKPAD_FORCE"); return;
	case XRT_INPUT_INDEX_TRACKPAD_TOUCH: DG("XRT_INPUT_INDEX_TRACKPAD_TOUCH"); return;
	case XRT_INPUT_INDEX_GRIP_POSE: DG("XRT_INPUT_INDEX_GRIP_POSE"); return;
	case XRT_INPUT_INDEX_AIM_POSE: DG("XRT_INPUT_INDEX_AIM_POSE"); return;
	case XRT_INPUT_VIVE_SYSTEM_CLICK: DG("XRT_INPUT_VIVE_SYSTEM_CLICK"); return;
	case XRT_INPUT_VIVE_SQUEEZE_CLICK: DG("XRT_INPUT_VIVE_SQUEEZE_CLICK"); return;
	case XRT_INPUT_VIVE_MENU_CLICK: DG("XRT_INPUT_VIVE_MENU_CLICK"); return;
	case XRT_INPUT_VIVE_TRIGGER_CLICK: DG("XRT_INPUT_VIVE_TRIGGER_CLICK"); return;
	case XRT_INPUT_VIVE_TRIGGER_VALUE: DG("XRT_INPUT_VIVE_TRIGGER_VALUE"); return;
	case XRT_INPUT_VIVE_TRACKPAD: DG("XRT_INPUT_VIVE_TRACKPAD"); return;
	case XRT_INPUT_VIVE_TRACKPAD_CLICK: DG("XRT_INPUT_VIVE_TRACKPAD_CLICK"); return;
	case XRT_INPUT_VIVE_TRACKPAD_TOUCH: DG("XRT_INPUT_VIVE_TRACKPAD_TOUCH"); return;
	case XRT_INPUT_VIVE_GRIP_POSE: DG("XRT_INPUT_VIVE_GRIP_POSE"); return;
	case XRT_INPUT_VIVE_AIM_POSE: DG("XRT_INPUT_VIVE_AIM_POSE"); return;
	case XRT_INPUT_VIVEPRO_SYSTEM_CLICK: DG("XRT_INPUT_VIVEPRO_SYSTEM_CLICK"); return;
	case XRT_INPUT_VIVEPRO_VOLUP_CLICK: DG("XRT_INPUT_VIVEPRO_VOLUP_CLICK"); return;
	case XRT_INPUT_VIVEPRO_VOLDN_CLICK: DG("XRT_INPUT_VIVEPRO_VOLDN_CLICK"); return;
	case XRT_INPUT_VIVEPRO_MUTE_MIC_CLICK: DG("XRT_INPUT_VIVEPRO_MUTE_MIC_CLICK"); return;
	case XRT_INPUT_WMR_MENU_CLICK: DG("XRT_INPUT_WMR_MENU_CLICK"); return;
	case XRT_INPUT_WMR_SQUEEZE_CLICK: DG("XRT_INPUT_WMR_SQUEEZE_CLICK"); return;
	case XRT_INPUT_WMR_TRIGGER_VALUE: DG("XRT_INPUT_WMR_TRIGGER_VALUE"); return;
	case XRT_INPUT_WMR_THUMBSTICK_CLICK: DG("XRT_INPUT_WMR_THUMBSTICK_CLICK"); return;
	case XRT_INPUT_WMR_THUMBSTICK: DG("XRT_INPUT_WMR_THUMBSTICK"); return;
	case XRT_INPUT_WMR_TRACKPAD_CLICK: DG("XRT_INPUT_WMR_TRACKPAD_CLICK"); return;
	case XRT_INPUT_WMR_TRACKPAD_TOUCH: DG("XRT_INPUT_WMR_TRACKPAD_TOUCH"); return;
	case XRT_INPUT_WMR_TRACKPAD: DG("XRT_INPUT_WMR_TRACKPAD"); return;
	case XRT_INPUT_WMR_GRIP_POSE: DG("XRT_INPUT_WMR_GRIP_POSE"); return;
	case XRT_INPUT_WMR_AIM_POSE: DG("XRT_INPUT_WMR_AIM_POSE"); return;
	case XRT_INPUT_XBOX_MENU_CLICK: DG("XRT_INPUT_XBOX_MENU_CLICK"); return;
	case XRT_INPUT_XBOX_VIEW_CLICK: DG("XRT_INPUT_XBOX_VIEW_CLICK"); return;
	case XRT_INPUT_XBOX_A_CLICK: DG("XRT_INPUT_XBOX_A_CLICK"); return;
	case XRT_INPUT_XBOX_B_CLICK: DG("XRT_INPUT_XBOX_B_CLICK"); return;
	case XRT_INPUT_XBOX_X_CLICK: DG("XRT_INPUT_XBOX_X_CLICK"); return;
	case XRT_INPUT_XBOX_Y_CLICK: DG("XRT_INPUT_XBOX_Y_CLICK"); return;
	case XRT_INPUT_XBOX_DPAD_DOWN_CLICK: DG("XRT_INPUT_XBOX_DPAD_DOWN_CLICK"); return;
	case XRT_INPUT_XBOX_DPAD_RIGHT_CLICK: DG("XRT_INPUT_XBOX_DPAD_RIGHT_CLICK"); return;
	case XRT_INPUT_XBOX_DPAD_UP_CLICK: DG("XRT_INPUT_XBOX_DPAD_UP_CLICK"); return;
	case XRT_INPUT_XBOX_DPAD_LEFT_CLICK: DG("XRT_INPUT_XBOX_DPAD_LEFT_CLICK"); return;
	case XRT_INPUT_XBOX_SHOULDER_LEFT_CLICK: DG("XRT_INPUT_XBOX_SHOULDER_LEFT_CLICK"); return;
	case XRT_INPUT_XBOX_SHOULDER_RIGHT_CLICK: DG("XRT_INPUT_XBOX_SHOULDER_RIGHT_CLICK"); return;
	case XRT_INPUT_XBOX_THUMBSTICK_LEFT_CLICK: DG("XRT_INPUT_XBOX_THUMBSTICK_LEFT_CLICK"); return;
	case XRT_INPUT_XBOX_THUMBSTICK_LEFT: DG("XRT_INPUT_XBOX_THUMBSTICK_LEFT"); return;
	case XRT_INPUT_XBOX_THUMBSTICK_RIGHT_CLICK: DG("XRT_INPUT_XBOX_THUMBSTICK_RIGHT_CLICK"); return;
	case XRT_INPUT_XBOX_THUMBSTICK_RIGHT: DG("XRT_INPUT_XBOX_THUMBSTICK_RIGHT"); return;
	case XRT_INPUT_XBOX_LEFT_TRIGGER_VALUE: DG("XRT_INPUT_XBOX_LEFT_TRIGGER_VALUE"); return;
	case XRT_INPUT_XBOX_RIGHT_TRIGGER_VALUE: DG("XRT_INPUT_XBOX_RIGHT_TRIGGER_VALUE"); return;
	case XRT_INPUT_GO_SYSTEM_CLICK: DG("XRT_INPUT_GO_SYSTEM_CLICK"); return;
	case XRT_INPUT_GO_TRIGGER_CLICK: DG("XRT_INPUT_GO_TRIGGER_CLICK"); return;
	case XRT_INPUT_GO_BACK_CLICK: DG("XRT_INPUT_GO_BACK_CLICK"); return;
	case XRT_INPUT_GO_TRACKPAD_CLICK: DG("XRT_INPUT_GO_TRACKPAD_CLICK"); return;
	case XRT_INPUT_GO_TRACKPAD_TOUCH: DG("XRT_INPUT_GO_TRACKPAD_TOUCH"); return;
	case XRT_INPUT_GO_TRACKPAD: DG("XRT_INPUT_GO_TRACKPAD"); return;
	case XRT_INPUT_GO_GRIP_POSE: DG("XRT_INPUT_GO_GRIP_POSE"); return;
	case XRT_INPUT_GO_AIM_POSE: DG("XRT_INPUT_GO_AIM_POSE"); return;
	case XRT_INPUT_TOUCH_X_CLICK: DG("XRT_INPUT_TOUCH_X_CLICK"); return;
	case XRT_INPUT_TOUCH_X_TOUCH: DG("XRT_INPUT_TOUCH_X_TOUCH"); return;
	case XRT_INPUT_TOUCH_Y_CLICK: DG("XRT_INPUT_TOUCH_Y_CLICK"); return;
	case XRT_INPUT_TOUCH_Y_TOUCH: DG("XRT_INPUT_TOUCH_Y_TOUCH"); return;
	case XRT_INPUT_TOUCH_MENU_CLICK: DG("XRT_INPUT_TOUCH_MENU_CLICK"); return;
	case XRT_INPUT_TOUCH_A_CLICK: DG("XRT_INPUT_TOUCH_A_CLICK"); return;
	case XRT_INPUT_TOUCH_A_TOUCH: DG("XRT_INPUT_TOUCH_A_TOUCH"); return;
	case XRT_INPUT_TOUCH_B_CLICK: DG("XRT_INPUT_TOUCH_B_CLICK"); return;
	case XRT_INPUT_TOUCH_B_TOUCH: DG("XRT_INPUT_TOUCH_B_TOUCH"); return;
	case XRT_INPUT_TOUCH_SYSTEM_CLICK: DG("XRT_INPUT_TOUCH_SYSTEM_CLICK"); return;
	case XRT_INPUT_TOUCH_SQUEEZE_VALUE: DG("XRT_INPUT_TOUCH_SQUEEZE_VALUE"); return;
	case XRT_INPUT_TOUCH_TRIGGER_TOUCH: DG("XRT_INPUT_TOUCH_TRIGGER_TOUCH"); return;
	case XRT_INPUT_TOUCH_TRIGGER_VALUE: DG("XRT_INPUT_TOUCH_TRIGGER_VALUE"); return;
	case XRT_INPUT_TOUCH_THUMBSTICK_CLICK: DG("XRT_INPUT_TOUCH_THUMBSTICK_CLICK"); return;
	case XRT_INPUT_TOUCH_THUMBSTICK_TOUCH: DG("XRT_INPUT_TOUCH_THUMBSTICK_TOUCH"); return;
	case XRT_INPUT_TOUCH_THUMBSTICK: DG("XRT_INPUT_TOUCH_THUMBSTICK"); return;
	case XRT_INPUT_TOUCH_THUMBREST_TOUCH: DG("XRT_INPUT_TOUCH_THUMBREST_TOUCH"); return;
	case XRT_INPUT_TOUCH_GRIP_POSE: DG("XRT_INPUT_TOUCH_GRIP_POSE"); return;
	case XRT_INPUT_TOUCH_AIM_POSE: DG("XRT_INPUT_TOUCH_AIM_POSE"); return;
	case XRT_INPUT_HAND_SELECT_VALUE: DG("XRT_INPUT_HAND_SELECT_VALUE"); return;
	case XRT_INPUT_HAND_SQUEEZE_VALUE: DG("XRT_INPUT_HAND_SQUEEZE_VALUE"); return;
	case XRT_INPUT_HAND_GRIP_POSE: DG("XRT_INPUT_HAND_GRIP_POSE"); return;
	case XRT_INPUT_HAND_AIM_POSE: DG("XRT_INPUT_HAND_AIM_POSE"); return;
	default: break;
	}

	uint32_t id = XRT_GET_INPUT_ID(name);
	enum xrt_input_type type = XRT_GET_INPUT_TYPE(name);
	const char *str = get_xrt_input_type_short_str(type);

	u_pp(dg, "XRT_INPUT_0x%04x_%s", id, str);
}
